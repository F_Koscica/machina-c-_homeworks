// MachinaZadatak2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <math.h>
#include <cstdio>

using namespace std;

int main(){

	//x1 = (-b + sqrt(b * b - 4 * a * c)) / (2 *a);
	//x2 = (-b - sqrt(b * b - 4 * a * c)) / (2 * a);
	
	double x1, x2, a, b, c;
	do {

		cout << "Enter the value of a = ";
		cin >> a;
		cout << "Enter the value of b = ";
		cin >> b;
		cout << "Enter the value of c = ";
		cin >> c;


		x1 = (-b + sqrt(b * b - 4 * a * c)) / (2 * a);
		x2 = (-b - sqrt(b * b - 4 * a * c)) / (2 * a);

		if ((b * b - 4 * a * c) < 0) {
			cout << "Unreal number. Root cannot be a negative number. " << endl;

			system("pause");
		}
		else if (a==0) {

			cout << "The value (a) cannot be 0. Cannot divide by 0. " << endl;

			system("pause");
		}
		else {
			cout << "The quadratic equation is: " << endl;
			cout <<"x1: "<< x1 << endl;
			cout <<"x2: "<< x2 << endl;

			system("pause");
		}

	} while (((b * b - 4 * a * c) < 0) || a == 0);
	
	return 0;
	
}

