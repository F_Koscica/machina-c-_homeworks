// MachinaZadatakFactorial.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

int calculateFactorial(int num, int factorial) {

	for (int i = 1; i <= num; i++) {

		factorial = factorial*i;
	}
	return factorial;
}

int main()
{
	int num;
	int factorial = 1;

	do{

	cout << "Enter a whole number: ";
	cin >> num;

	if (num < 0) {
		cout << "The number cannot be a negative number." << endl;
	}
	else {
		calculateFactorial(num, factorial);

	}
	} while (num < 0);

	int fact = calculateFactorial(num, factorial);

	cout << "Factorial of number " << num << " is: " << fact << endl;

	system("pause");
    return 0;
}

