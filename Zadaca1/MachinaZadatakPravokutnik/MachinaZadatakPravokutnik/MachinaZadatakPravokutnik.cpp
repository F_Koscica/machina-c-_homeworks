// MachinaZadatakPravokutnik.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cstdio>

using namespace std;

int dotShapeRelationship(float dotX,float dotY, float sLength, float sHeight) {

	//P= ab

	float dotLocation = dotX * dotY;
	float shape = sLength* sHeight;

	if (dotLocation < shape) {
		return 1;
	}
	if (dotLocation > shape) {
		return -1;
	}
	else {
		return 0;
	}	

}
int main(){

	float sLength;
	float sHeight;
	float dotX;
	float dotY;

	int relationship;

	do {

		cout << "Enter your shapes perimeters!" << endl;
		cout << "Length: ";
		cin >> sLength;
		cout << "Height: ";
		cin >> sHeight;

		cout << "Enter the location of your dot!" << endl;
		cout << "Dot location X: ";
		cin >> dotX;
		cout << "Dot location Y: ";
		cin >> dotY;

		relationship = dotShapeRelationship(dotX, dotY, sLength, sHeight);

		if (relationship < 0) {
			cout << "Dot is outside the object." << endl;
		}

		if (relationship >= 0) {
			cout << "Dot is inside the object." << endl;
		}

	} while (relationship < 0);

	system("pause");
    return 0;
}

