// MachinaZadatakRealniBrojevi.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <math.h>
 
using namespace std;

double calculateNumbers(int numberAmount, double realNumber[]) {

	double sumOfNumbers = 0.0;
	for (int i = 0; i < numberAmount; i++) {

		sumOfNumbers += realNumber[i];	
	}
	return sumOfNumbers;
	
	
}



double calculateArithmeticAverage(int numberAmount, double realNumber[]) {

	double sumOfNumbers = 0.0;
	for (int i = 0; i < numberAmount; i++) {

		sumOfNumbers += realNumber[i];
	}
	double arithmeticAverage = sumOfNumbers / numberAmount;

	return arithmeticAverage;
}



double calculateGeoAverage(int numberAmount, double realNumber[]) {

	double geometricAverage = 0.0;
	double geometricMean = 0.0;
	for (int i = 0; i < numberAmount; i++) {

		double realNumberValue = realNumber[i];
	
		geometricMean = realNumberValue * realNumberValue;

		geometricAverage = pow(geometricMean,( 1.0 / i));
	}
	return  geometricAverage;
}



int main(){

	const int size = 1000;
	double realNumber[size];


	int numberAmount;
	cout << "Enter the amount of real numbers you would like to use: " ;
	cin >> numberAmount;


	cout << "Enter the real numbers you would like to use: " << endl;

	int index = 0;
	while(index < numberAmount){
		double numberValue;
		cin >> numberValue;
		realNumber[index] = numberValue;

		index++;
	};
	
	for (int i = 0; i < numberAmount; i++) {
		cout << "Value of [" << i << "]: " << realNumber[i] << endl;
		
	}
	
	double sumResult = calculateNumbers(numberAmount, realNumber);
	double arithmeticResult = calculateArithmeticAverage(numberAmount,realNumber);
	double geometricResult = calculateGeoAverage(numberAmount, realNumber);

	cout << "The sum of numbers is: " << sumResult << endl;
	cout << "The arithmetic average is: " << arithmeticResult << endl;
	cout << "The geometric average is: " << geometricResult << endl;

	system("pause");
    return 0;
}

